package sorting;

public class InsertionSort implements Sort{
    @Override
    public int[] sort(int[] a, StepAction sa){
	for(int i=1; i<a.length; i++){
	    int key = a[i];
	    int j = i-1;
	    while(j>=0 && a[j] > key){
		a[j+1] = a[j];
		j --;
	    }
	    a[j+1] = key;
	    sa.doAction();
	}
	return a;
    }
    @Override
    public String toString(){
	return "Insertion sort";
    }
}
