package sorting;

import java.util.HashSet;

public class SortFactory{
    private HashSet<Sort> set;
        
    public SortFactory(){
	set = new HashSet<Sort>();
	set.add(new BubbleSort());
	set.add(new SelectionSort());
	set.add(new InsertionSort());
	set.add(new MergeSort());
	set.add(new HeapSort());
	//set.add(new .....Sort());
    }

    public int nbrOfSortingMethod(){
	return set.size();
    }

    public HashSet<Sort> getAllSort(){
	return set;
    }

}
