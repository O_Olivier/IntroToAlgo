package sorting;

public class HeapSort implements Sort{
    @Override
    public int[] sort(int[] a, StepAction sa){
	MaxHeap mh = new MaxHeap(a);
	for(int i=a.length-1; i > 0; i--){
	    ArrayUtilities.swap(a, 0, i);
	    mh.decrement_heapsize();
	    mh.max_heapify(a, 1);
	    sa.doAction();
	}
	return a;
    }

    @Override
    public String toString(){
	return "Heapsort";
    }
}
