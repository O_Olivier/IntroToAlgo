package view;

import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;

import sorting.SortingRunnable;
import sorting.Sort;

public class SortProcessView extends JPanel{
    private int[] array;
    private int max_value;

    public SortProcessView(int array_length, int max_value, Sort sort){
	this.max_value = max_value;
        array = new int[array_length];
	for(int j=0; j<array_length;j++){
	    array[j] = (int)(Math.random()*max_value);
	}
	(new Thread(new SortingRunnable(sort, array, this))).start();
    }

    public void paintComponent(Graphics g){
	super.paintComponent(g);
	int a = (int)((double)(array.length)/(double)(this.getWidth()));
	float b = (float)this.getHeight()/(float)(max_value-1);
	g.setColor(Color.RED);
	for(int i=0; i<this.getWidth()-1; i++){
	    if(i*a < array.length){
		g.fillRect(i, this.getHeight()-(int)(array[i*a]*b), 1, (int)(array[i*a]*b));
		//g.fillRect(i, this.getHeight()-(int)((float)(array[i*a])*b)-(this.getHeight()/10), 1, this.getHeight()/10);
	    }
	}
    }
}
