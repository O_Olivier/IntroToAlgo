package view;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.BoxLayout;
import javax.swing.JButton;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;

import sorting.Sort;

public class SortOptions extends JPanel{
    private OptionPanel sizeOfArray;
    private OptionPanel maxValue;
    private OptionPanel timeSleptByStep;
    private JButton launch;
    private SortProcessView spv;

    public SortOptions(Sort sort){
	this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	sizeOfArray = new OptionPanel("Size of the array : ", "100000");
	maxValue = new OptionPanel("Max value in the array : ", "100");
	timeSleptByStep = new OptionPanel("Time slept each step of the sorting process : ", "0");

	this.add(sizeOfArray);
	this.add(maxValue);
	this.add(timeSleptByStep);
	
	launch = new JButton("Launch Sorting process");
	launch.addActionListener(e -> launchSortProcess(sort));
	this.add(launch);
    }

    public void launchSortProcess(Sort sort){
	for(Component c: this.getComponents()){
	    c.setVisible(false);
	}
	//this.getParent().setLayout(new FlowLayout());
        spv = new SortProcessView(sizeOfArray.getText(),maxValue.getText(),sort);
	this.getParent().add(spv);
    }

    @Override
    public void paintComponent(Graphics g){
	super.paintComponent(g);
	if(spv != null){
	    spv.setSize(this.getWidth(), this.getHeight());
	}
    }

    class OptionPanel extends JPanel{
	private JTextArea immutableText;
	private JTextArea text;

	public OptionPanel(String s, String defaultValue){
	    immutableText = new JTextArea(s);
	    immutableText.setEditable(false);
	    text = new JTextArea(defaultValue);
	    text.setColumns(20);
	    this.add(immutableText);
	    this.add(text);
	}

	public int getText(){
	    return Integer.parseInt(text.getText());
	}
    }
}
