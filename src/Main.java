import sorting.Sort;
import sorting.SortFactory;
import sorting.SortingRunnable;
import view.View;

import java.util.HashSet;
import java.util.Iterator;

public class Main{
    public static final int ARRAY_LENGTH = 100_000;
    
    public static void main(String[] args)
	throws InterruptedException{
	
	final int MAX_VALUE;
	if(args.length > 0){
	    MAX_VALUE = Integer.parseInt(args[0]);
	}else{
	    MAX_VALUE = 100;
	}
	
	int[] unsortedArray = new int[ARRAY_LENGTH];
	for(int j=0; j<ARRAY_LENGTH;j++){
	    unsortedArray[j] = (int)(Math.random()*MAX_VALUE);
	}
	View v = new View();
	/*
	View v = new View(unsortedArray, MAX_VALUE);
	
	SortFactory factory = new SortFactory();
	int nbrSortMethod = factory.nbrOfSortingMethod();

	HashSet<int[]> copies = new HashSet<int[]>();
	for(int i=0; i<nbrSortMethod; i++){
	    copies.add(unsortedArray.clone());
	}

	Iterator<int[]> arrayCopiesIt = copies.iterator();
	Iterator<Sort> sortMethodIt = factory.getAllSort().iterator();
	
	for(int i=0; i<nbrSortMethod; i++){
	    int[] currentCopy = arrayCopiesIt.next();
	    v.setArray(currentCopy);
	    Thread t = new Thread(new SortingRunnable(sortMethodIt.next(), currentCopy, v));
	    t.start();
	    t.join();
	}
	
	System.exit(0);*/
	

	//Exercice 2-4
	/*
	System.out.println((new Inversions()).countInversions(new int[]{2,3,8,6,1,0,-1}));
	*/
    }
}
